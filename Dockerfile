FROM adoptopenjdk/openjdk14:alpine-slim AS build
WORKDIR  /app
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
RUN  chmod +x mvnw && ./mvnw dependency:go-offline
COPY src src
RUN ./mvnw package




FROM adoptopenjdk/openjdk14:alpine-slim AS prod
WORKDIR  /app
COPY --from=build /app/target/*.jar app.jar
RUN mkdir app-fs
EXPOSE  8080
ENTRYPOINT ["java","-jar","app.jar"]
