package ma.octo.assignement.domain;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

@Entity
@DiscriminatorValue("VERSEMENT")
public class Versement extends Operation{

    @JoinColumn(name = "emetteur")
    private String nomEmetteur;

    public String getNomEmetteur() {
        return nomEmetteur;
    }

    public void setNomEmetteur(String nomEmetteur) {
        this.nomEmetteur = nomEmetteur;
    }
}
