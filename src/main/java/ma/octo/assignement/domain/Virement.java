package ma.octo.assignement.domain;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@DiscriminatorValue("VIREMENT")
public class Virement extends Operation{
    @ManyToOne
    @JoinColumn(name = "emetteur")
    private Compte compteEmetteur;

    public Compte getCompteEmetteur() {
        return compteEmetteur;
    }

    public void setCompteEmetteur(Compte compteEmetteur) {
        this.compteEmetteur = compteEmetteur;
    }
}
