package ma.octo.assignement.service;

import ma.octo.assignement.IService.ICompte;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Transactional
@Service
public class CompteService implements ICompte {
    @Autowired
    private CompteRepository compteRepository;

    @Override
    public List<Compte> loadAllCompte() {
        List<Compte> all = compteRepository.findAll();
        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }
}
