package ma.octo.assignement.service;

import ma.octo.assignement.IService.IOperation;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.OperationRepository;
import ma.octo.assignement.web.OperationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class OperationService implements IOperation {
    @Autowired
    private OperationRepository operationRepository;
    @Autowired
    private CompteRepository compteRepository;
    Logger LOGGER = LoggerFactory.getLogger(OperationController.class);
    @Autowired
    private AuditService auditService;

    @Value("${MONTANT_MAXIMAL_VIREMENT}")
    private int MONTANT_MAXIMAL_VIREMENT;
    @Value("${MONTANT_MINIMAL_VIREMENT}")
    private int MONTANT_MINIMAL_VIREMENT;

    @Value("${MONTANT_MAXIMAL_VERSEMENT}")
    private int MONTANT_MAXIMAL_VERSEMENT;
    @Value("${MONTANT_MINIMAL_VERSEMENT}")
    private int MONTANT_MINIMAL_VERSEMENT;


    @Override
    public List<Virement> loadAllVirement() {
        List<Virement> all=new ArrayList<Virement>();
        operationRepository.findAll()
                .stream()
                .filter(op -> op.getClass()==Virement.class)
                .forEach(op->all.add((Virement) op));
        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @Override
    public List<Versement> loadAllVersement() {
        List<Versement> all=new ArrayList<Versement>();
        operationRepository.findAll()
                .stream()
                .filter(op -> op.getClass()==Versement.class)
                .forEach(op->all.add((Versement) op));
        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }


    @Override
    public void faireVirement(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        Compte emetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte beneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (emetteur == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (beneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (virementDto.getMontantVirement() == null) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() < MONTANT_MINIMAL_VIREMENT) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL_VIREMENT) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif().isEmpty()) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (emetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }


        emetteur.setSolde(emetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteRepository.save(emetteur);

        beneficiaire.setSolde(new BigDecimal(beneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
        compteRepository.save(beneficiaire);

        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(beneficiaire);
        virement.setCompteEmetteur(emetteur);
        virement.setMotif(virementDto.getMotif());
        virement.setMontant(virementDto.getMontantVirement());


        operationRepository.save(virement);

        auditService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString());
    }

    @Override
    public void faireVersement(VersementDto versementDto) throws CompteNonExistantException, TransactionException {
        Compte beneficiaire = compteRepository.findByRib(versementDto.getRibBeneficiaire());


        if (beneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (versementDto.getMontantVersement() == null) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() < MONTANT_MINIMAL_VERSEMENT) {
            System.out.println("Montant minimal de versement non atteint");
            throw new TransactionException("Montant minimal de versement non atteint");
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL_VERSEMENT) {
            System.out.println("Montant maximal de versement dépassé");
            throw new TransactionException("Montant maximal de versement dépassé");
        }

        if (versementDto.getMotif().isEmpty()) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }


        beneficiaire.setSolde(new BigDecimal(beneficiaire.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(beneficiaire);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDate());
        versement.setCompteBeneficiaire(beneficiaire);
        versement.setNomEmetteur(versementDto.getNomEmetteur());
        versement.setMotif(versementDto.getMotif());
        versement.setMontant(versementDto.getMontantVersement());


        operationRepository.save(versement);

        auditService.auditVersement("Versement depuis " + versementDto.getNomEmetteur() + " vers " + versementDto
                .getMontantVersement() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());
    }
}
