package ma.octo.assignement.IService;

import ma.octo.assignement.domain.Compte;

import java.util.List;

public interface ICompte {
    List<Compte> loadAllCompte();
}
