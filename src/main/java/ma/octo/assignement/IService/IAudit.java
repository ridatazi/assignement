package ma.octo.assignement.IService;

public interface IAudit {
    void auditVirement(String message);
    void auditVersement(String message);
}
