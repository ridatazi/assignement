package ma.octo.assignement.IService;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IOperation {
    List<Virement> loadAllVirement();
    List<Versement> loadAllVersement();
    void faireVirement(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;
    void faireVersement(VersementDto versementDto) throws CompteNonExistantException, TransactionException;
}
