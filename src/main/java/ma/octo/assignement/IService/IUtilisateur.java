package ma.octo.assignement.IService;

import ma.octo.assignement.domain.Utilisateur;

import java.util.List;

public interface IUtilisateur {
    List<Utilisateur> loadAllUtilisateur();
}
