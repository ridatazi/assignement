package ma.octo.assignement.web;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/compte")
public class CompteController {
    @Autowired
    private CompteService compteService;

    @GetMapping("lister_all")
    List<Compte> loadAllCompte() {
        return compteService.loadAllCompte();
    }
}
