package ma.octo.assignement.web;


import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/operation")
public class OperationController {
    @Autowired
    private OperationService operationService;

    @GetMapping("lister_virements")
    List<Virement> loadAllVirement() {
        return operationService.loadAllVirement();
    }

    @GetMapping("lister_versements")
    List<Versement> loadAllVersement() {
        return operationService.loadAllVersement();
    }

    @PostMapping("/executerVirement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        operationService.faireVirement(virementDto);
    }

    @PostMapping("/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody VersementDto versementDto)
            throws  CompteNonExistantException, TransactionException {
        operationService.faireVersement(versementDto);
    }

}