package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class VersementDto {
  private String NomEmetteur;
  private String ribBeneficiaire;
  private String motif;
  private BigDecimal montantVersement;
  private Date date;

  public String getNrCompteEmetteur() {
    return NomEmetteur;
  }

  public void setNrCompteEmetteur(String NomEmetteur) {
    this.NomEmetteur = NomEmetteur;
  }

  public String getNomEmetteur() {
    return NomEmetteur;
  }

  public void setNomEmetteur(String nomEmetteur) {
    NomEmetteur = nomEmetteur;
  }

  public String getRibBeneficiaire() {
    return ribBeneficiaire;
  }

  public void setRibBeneficiaire(String ribBeneficiaire) {
    this.ribBeneficiaire = ribBeneficiaire;
  }

  public BigDecimal getMontantVersement() {
    return montantVersement;
  }

  public void setMontantVersement(BigDecimal montantVersement) {
    this.montantVersement = montantVersement;
  }

  public String getMotif() {
    return motif;
  }

  public void setMotif(String motif) {
    this.motif = motif;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }
}
